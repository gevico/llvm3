Low Level Virtual Machine (LLVM)
================================

This directory and its subdirectories contain source code for the Low Level
Virtual Machine, a toolkit for the construction of highly optimized compilers,
optimizers, and runtime environments.

LLVM is open source software. You may freely distribute it under the terms of
the license agreement found in LICENSE.txt.

Please see the HTML documentation provided in docs/index.html for further
assistance with LLVM.

If you're writing a package for LLVM, see docs/Packaging.html for our
suggestions.


## linux:
```bash
./configure && make -j8 && make install
```

## mingw64-make:
```bash
mkdir build && cd build \
&& cmake .. -G "MinGW Makefiles" -DCMAKE_MAKE_PROGRAM=mingw32-make.exe -DMSYS=ON -DLLVM_ENABLE_ASSERTIONS=ON -DCMAKE_INSTALL_PREFIX=/mingw64/ \
&& cmake --build .
```

## mingw64-ninja:
```bash
mkdir build && cd build \
cmake .. -G "Ninja" -DMSYS=ON -DLLVM_ENABLE_ASSERTIONS=ON -DCMAKE_INSTALL_PREFIX=/mingw64/ \
&& cmake --build .
```

Modify llvm-config.in:

```c
// build\tools\llvm-config\llvm-config.in
- #!D:/app/msys2/usr/bin/perl.exe
+ #!/usr/bin/perl.exe
```

install:

```bash
make install
```